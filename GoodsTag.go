package main

import (
	"fmt"
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"
)

const nextLineUtf8 = "\r\n"

const separatorSpaced = " = "
const separator = "="

const (
	engWindowName string = "Goods Tag"
	engTag        string = "Tag"
	engProduct    string = "Product"
	engTags       string = "Tags"
)

var (
	locWindowName string = engWindowName
	locTag        string = engTag
	locProduct    string = engProduct
	locTags       string = engTags
)

type TableGood struct {
	id   int
	name string
	tags string
}

type TableModel struct {
	walk.TableModelBase
	walk.SorterBase
	sortColumn int
	sortOrder  walk.SortOrder
	goods      []*TableGood
}

func NewTableModel(goods []Good) *TableModel {
	m := new(TableModel)
	m.ResetRows(goods)
	return m
}

// Called by the TableView from SetModel and every time the model publishes a
// RowsReset event.
func (m *TableModel) RowCount() int {
	return len(m.goods)
}

// Called by the TableView when it needs the text to display for a given cell.
func (m *TableModel) Value(row, col int) interface{} {
	item := m.goods[row]

	switch col {
	case 0:
		return item.id
	case 1:
		return item.name
	case 2:
		return item.tags
	}

	panic("unexpected col")
}

// Called by the TableView to sort the model.
func (m *TableModel) Sort(col int, order walk.SortOrder) error {
	m.sortColumn, m.sortOrder = col, order

	sort.Sort(m)

	return m.SorterBase.Sort(col, order)
}

func (m *TableModel) Len() int {
	return len(m.goods)
}

func (m *TableModel) Less(i, j int) bool {
	a, b := m.goods[i], m.goods[j]

	c := func(ls bool) bool {
		if m.sortOrder == walk.SortAscending {
			return ls
		}

		return !ls
	}

	switch m.sortColumn {
	case 0:
		return c(a.id < b.id)
	case 1:
		return c(a.name < b.name)
	case 2:
		return c(a.tags < b.tags)
	}

	panic("unreachable")
}

func (m *TableModel) Swap(i, j int) {
	m.goods[i], m.goods[j] = m.goods[j], m.goods[i]
}

func (m *TableModel) ResetRows(goods []Good) {
	m.goods = make([]*TableGood, len(goods))

	for i, good := range goods {
		var tagStringed string
		sort.Strings(good.tags)
		for _, tag := range good.tags {
			tagStringed = tagStringed + tag + " "
		}
		m.goods[i] = &TableGood{
			id:   good.id,
			name: good.name,
			tags: tagStringed,
		}
	}

	m.PublishRowsReset()

	m.Sort(m.sortColumn, m.sortOrder)
}

type Good struct {
	id   int
	name string
	tags []string
}

var gGoods []Good
var gTags map[string][]int

func init() {
	//MAIN LIST OF GOODS:
	//id name
	if data, err := ioutil.ReadFile("goods/price.txt"); err != nil {
		log.Fatal(err)
	} else {
		if s, err2 := byteToUtf8toString(data); err2 != nil {
			log.Printf("%v\n", err2)
		} else {
			gGoods = parsePriceToGoods(gGoods, s)
		}
	}
	//CUSTOM LIST OF GOODS:
	//id name tag1, tag2, tag3, tag4, ...
	if data, err := ioutil.ReadFile("goods/database.txt"); err != nil {
		//		log.Fatal(err)
		log.Printf("%v\n", err)
	} else {
		if s, err2 := byteToUtf8toString(data); err2 != nil {
			log.Printf("%v\n", err2)
		} else {
			gGoods = parseDataBaseToGoods(gGoods, s)
		}
	}
	//CUSTOM LIST OF TAGS:
	//id tag1, tag2, tag3, tag4, ...
	if data, err := ioutil.ReadFile("goods/tags.txt"); err != nil {
		log.Printf("%v\n", err)
	} else {
		if s, err2 := byteToUtf8toString(data); err2 != nil {
			log.Printf("%v\n", err2)
		} else {
			gTags = parseTagsData(s)
		}
	}
	//LOCALIZATION:
	//var = "translation"
	if localizations, err := ioutil.ReadDir("localization"); err != nil {
		log.Printf("%v\n", err)
	} else {
		log.Println("loading localizations...")
		//TODO: add a menu where user can select language and add an ini file with user settings
		if len(localizations) > 0 && strings.ToLower(localizations[0].Name()) == "localization.txt" {
			if data, err := ioutil.ReadFile("localization/localization.txt"); err != nil {
				log.Printf("%v\n", err)
			} else {
				if s, err2 := byteToUtf8toString(data); err2 != nil {
					log.Printf("%v\n", err2)
				} else {
					setLocalizationData(s)
				}
			}
		}
	}
	gTags = appendTagsFromGoods(gTags, gGoods)
	gGoods = appendTagsToGoods(gTags, gGoods)
}

func main() {
	//	var outTE *walk.TextEdit
	var inLE *walk.LineEdit

	model := NewTableModel(gGoods)

	log.Println("main")
	log.Printf("amount of goods: %d\n", len(gGoods))

	if _, err := (MainWindow{
		Title:   locWindowName,
		MinSize: Size{455, 400},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{Columns: 3},
				Children: []Widget{
					Label{
						Text: fmt.Sprintf("%s:", locTag),
					},
					LineEdit{
						AssignTo: &inLE,
						OnTextChanged: func() {
							goods := []Good{}
							//							goodsStringed := ""
							listedGoods := make(map[int]bool)
							inLEText := strings.ToLower(inLE.Text())
							if inLEText == "" {
								for _, good := range gGoods {
									goods = append(goods, good)
									//									goodsStringed = goodsStringed + good.name + nextLineUtf8
								}
								log.Printf("goods: %v\n", gGoods)
							} else {
								for key, _ := range gTags {
									tags := make([]string, 2, 2)
									tags[0] = strings.ToLower(key)
									tags[1] = inLEText
									prefix := commonPrefix(tags)
									log.Printf("key: %s\n", key)
									log.Printf("inLEText: %s\n", inLEText)
									log.Printf("prefix: %s\n", prefix)
									if prefix == "" {
										log.Printf("empty prefix\n\n")
										continue
									}
									if utf8.RuneCountInString(tags[0]) < utf8.RuneCountInString(tags[1]) {
										log.Printf("key < inLEText\n\n")
										continue
									}
									if utf8.RuneCountInString(prefix) < utf8.RuneCountInString(tags[1]) {
										log.Printf("prefix < inLEText\n\n")
										continue
									}
									if utf8.RuneCountInString(tags[0]) == utf8.RuneCountInString(tags[1]) && tags[0] != tags[1] {
										log.Printf("key == inLEText\n\n")
										continue
									}
									/*if prefix == key {
										goods = []Good{}
										for _, id := range gTags[key] {
											for _, good := range gGoods {
												if good.id == id {
													goods = append(goods, good)
													break
												}
											}
										}
										break
									}*/
									for _, id := range gTags[key] {
										for _, good := range gGoods {
											var found bool
											_, found = listedGoods[id]
											if good.id == id && !found {
												goods = append(goods, good)
												listedGoods[id] = true
											}
										}
									}
								}
								/*								for _, good := range goods {
																goodsStringed = goodsStringed + good.name + nextLineUtf8
															}*/
								log.Printf("amount of goods: %d\n", len(goods))
								//log.Printf("goods: %v\n", goods)
							}
							//							outTE.SetText(goodsStringed)
							model.ResetRows(goods)
						},
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					/*					TextEdit{
											AssignTo: &outTE,
											ReadOnly: true,
										},
					*/
					TableView{
						AlternatingRowBGColor: walk.RGB(223, 255, 223),
						ColumnsOrderable:      true,
						Columns: []TableViewColumn{
							{Title: "#", Width: 40},
							{Title: locProduct, Width: 170},
							{Title: locTags, Width: 170},
						},
						Model: model,
					},
				},
			},
		},
	}.Run()); err != nil {
		log.Fatal(err)
	}
}

func byteToUtf8toString(b []byte) (string, error) {
	/*	var s string
		for utf8.RuneCount(b) > 1 {
			if utf8.FullRune(b) {
				r, size := utf8.DecodeRune(b)
				s += string(r)
				b = b[size:]
			} else {
				break
			}
		}
		return s, nil*/
	/*	var buf = bytes.NewBuffer(make([]byte, len(b)*4))
		var bs []byte
		for _, br := range b {
			bs = append(bs, br)
			if utf8.FullRune(bs) {
				r := rune(br)
				buf.WriteRune(r)
				bs = []byte{}
			}
		}
		return buf.String(), nil*/
	return string(b), nil
}

func parsePriceToGoods(goods []Good, data string) []Good {
	var good Good
	log.Println("parseDataBaseToGoods")
	for _, line := range strings.Split(data, nextLineUtf8) {
		line = strings.TrimSpace(line)
		log.Println(line)
		if line == "" || strings.Count(line, "\t") < 2 {
			log.Println("skipping...")
			continue
		}
		for i, word := range strings.Split(line, "\t") {
			if i == 0 {
				if strings.Contains(word, "\ufeff") {
					word = word[len("\ufeff"):]
				}
				if id, err := strconv.Atoi(word); err != nil {
					log.Printf("failed to parse id of the good from \"%s\": %v\n",
						line, err)
					good.id = -1
					break
				} else {
					good.id = id
				}
			} else if i == 1 {
				good.name = word
			} else if i > 1 {
				break
			}
		}
		if good.id != -1 && good.name != "" {
			goods = append(goods, good)
			good = Good{}
		}
	}
	log.Printf("%v\n", goods)
	log.Printf("amount of goods: %d\n", len(goods))
	return goods
}

func parseDataBaseToGoods(goods []Good, data string) []Good {
	var good Good
	log.Println("parseDataBaseToGoods")
	for _, line := range strings.Split(data, nextLineUtf8) {
		line = strings.TrimSpace(line)
		log.Println(line)
		if line == "" || strings.Count(line, " ") < 2 {
			log.Println("skipping...")
			continue
		}
		for i, word := range strings.Split(line, " ") {
			if i == 0 {
				if strings.Contains(word, "\ufeff") {
					word = word[len("\ufeff"):]
				}
				if id, err := strconv.Atoi(word); err != nil {
					log.Printf("failed to parse id of the good from \"%s\": %v\n",
						line, err)
					good.id = -1
					break
				} else {
					good.id = id
					for _, good2 := range goods {
						if good2.id == good.id {
							good.id = -1
							break
						}
					}
				}
			} else if i == 1 {
				good.name = word
			} else if i > 1 {
				good.tags = append(good.tags, strings.ToLower(word))
			}
		}
		if good.id != -1 && good.name != "" {
			goods = append(goods, good)
			good = Good{}
		}
	}
	log.Printf("%v\n", goods)
	return goods
}

func parseTagsData(data string) map[string][]int {
	tags := make(map[string][]int)
	log.Println("parseTagsData")
	for _, line := range strings.Split(data, nextLineUtf8) {
		var id int
		line = strings.TrimSpace(line)
		log.Println(line)
		if line == "" || strings.Count(line, " ") < 1 {
			log.Println("skipping...")
			continue
		}
		for i, word := range strings.Split(line, " ") {
			if i == 0 {
				if strings.Contains(word, "\ufeff") {
					word = word[len("\ufeff"):]
				}
				var err error
				if id, err = strconv.Atoi(word); err != nil {
					log.Printf("failed to parse id of the good from \"%s\": \"%v\"", line, err)
					id = -1
					break
				}
			} else {
				if _, found := tags[word]; !found {
					tags[word] = []int{id}
				}
				for _, tagId := range tags[word] {
					if tagId == id {
						goto skipSameIdAppending
					}
				}
				tags[word] = append(tags[word], id)
			skipSameIdAppending:
			}
		}
	}
	log.Printf("tags: %v\n", tags)
	return tags
}

func setLocalizationData(data string) {
	for i, line := range strings.Split(data, nextLineUtf8) {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		if i == 0 && strings.Contains(line, "\ufeff") {
			line = line[len("\ufeff"):]
		}
		if strings.HasPrefix(line, "WindowName") {
			locWindowName = parseEqualSeparator(line, engWindowName)
			log.Printf("locWindowName: %s\n", locWindowName)
		} else if strings.HasPrefix(line, "Tags") {
			locTags = parseEqualSeparator(line, engTags)
		} else if strings.HasPrefix(line, "Tag") {
			locTag = parseEqualSeparator(line, engTag)
		} else if strings.HasPrefix(line, "Product") {
			locProduct = parseEqualSeparator(line, engProduct)
		}
	}
}

func parseEqualSeparator(line, defaultValue string) (out string) {
	log.Println("parseEqualSeparator")
	if i := strings.Index(line, separatorSpaced); i > -1 {
		out = line[i+len(separatorSpaced):]
	} else if i = strings.Index(line, separator); i > -1 {
		out = line[i+len(separator):]
	} else {
		log.Printf("using default localization for %s: %s\n", line, defaultValue)
		out = defaultValue
	}
	out = strings.TrimFunc(out, func(r rune) bool { return r == '"' })
	return out
}

func appendTagsFromGoods(tags map[string][]int, goods []Good) map[string][]int {
	log.Println("appendTagsFromGoods")
	for _, good := range goods {
		if len(good.tags) < 1 {
			continue
		}
		for _, tag := range good.tags {
			if _, found := tags[tag]; !found {
				tags[tag] = []int{}
			}
			for _, id := range tags[tag] {
				if id == good.id {
					goto nextGood
				}
			}
			tags[tag] = append(tags[tag], good.id)
		nextGood:
		}
	}
	log.Printf("%v\n", goods)
	//	log.Printf("%v\n", tags)
	return tags
}

//I know this func goes crazy...
func appendTagsToGoods(tags map[string][]int, goods []Good) []Good {
	log.Println("appendTagsFromGoods")
	//log.Printf("%v\n", tags)
	for tag, tagIds := range tags {
		for _, tagId := range tagIds {
			for i, good2 := range goods {
				if good2.id == tagId {
					found := false
					for _, tag2 := range good2.tags {
						if tag2 == tag {
							found = true
						}
					}
					if !found {
						log.Printf("appending tag: %s\n", tag)
						goods[i].tags = append(goods[i].tags, tag)
					}
				}
			}
		}
	}
	log.Printf("%v\n", goods)
	return goods
}

func commonPrefix(in []string) string {
	first := in[0]
	if utf8.RuneCountInString(first) <= 0 {
		return ""
	}
	inIn := make([]string, len(in))
	firstRune, firstSize := utf8.DecodeRuneInString(first)
	inIn[0] = first[firstSize:]
	for i := 1; i < len(in); i++ {
		next := in[i]
		nextRune, _ := utf8.DecodeRuneInString(next)
		if utf8.RuneCountInString(next) <= 0 || firstRune != nextRune {
			return ""
		}
		inIn[i] = next[firstSize:]
	}
	return string(firstRune) + commonPrefix(inIn)
}
